package kata.problema1

import scala.annotation.tailrec

sealed trait List[+T] {
  /**
   * Dându-se o funcție de mapare de la valori de tip T la
   * valori de tip U, transformă Lista sursă într-o Listă
   * de elemente de tip U.
   */
  def map[U](f: T => U): List[U] = {
    @tailrec
    def iter(xx: List[T], acc: List[U]): List[U] =
      xx match {
        case Nil => acc
        case Cons(x, xs) => iter(xs, f(x) :: acc)
      }
    iter(this, Nil).reverse
  }

  /**
   * Dându-se o funcție ce mapează elemente de tip T
   * la Liste de elemente de U, returnează o Listă
   * de elemente de tip U.
   */
  def flatMap[U](f: T => List[U]): List[U] = this.map(f).flatten

  /**
   * Dacă Lista sursă este o Listă de Liste cu elemente
   * de tip U, atunci concatenează Listele și returnează
   * rezultatul.
   */
  def flatten[U](implicit ev: T <:< List[U]): List[U] = {
    @tailrec
    def iter(xxs: List[T], acc: List[U]): List[U] = {
      xxs match {
        case Nil => acc
        case Cons(xs, xss) => iter(xss, xs ++ acc)
      }
    }
    iter(this.reverse, Nil)
  }

  /**
   * Dându-se o funcție predicat ce returnează adevăra sau
   * fals pentru fiecare element, returnează o nouă Listă ce
   * conține doar elementele pentru care predicatul a fost
   * adevărat.
   */
  def filter(p: T => Boolean): List[T] = {
    @tailrec
    def iter(xx: List[T], acc: List[T]): List[T] =
      xx match {
        case Nil => acc
        case Cons(x, xs) => iter(xs, if (p(x)) (x :: acc) else acc)
      }
    iter(this, Nil).reverse
  }

  /**
   * Returnează Lista sursă cu elementele în ordine inversă.
   */
  def reverse: List[T] = {
    @tailrec
    def iter(xx: List[T], acc: List[T]): List[T] =
      xx match {
        case Nil => acc
        case Cons(x, xs) => iter(xs, Cons(x, acc))
      }
    iter(this, Nil)
  }

  /**
   * Prefixează Lista sursă cu elementul dat.
   */
  def ::[U >: T](head: U): List[U] = {
    Cons(head, this)
  }

  /**
   * Prefixează Lista sursă cu elementul dat.
   */
  def +:[U >: T](head: U): List[U] = {
    Cons(head, this)
  }

  /**
   * Adaugă la sfârșit elementul dat.
   */
  def :+[U >: T](elem: U): List[U] = {
    (elem :: this.reverse).reverse
  }

  /**
   * Appends two Lists.
   */
  def ++[U >: T](ys: List[U]): List[U] = {
    @tailrec
    def iter(xx: List[U], acc: List[U]): List[U] =
      xx match {
        case Nil => acc
        case Cons(x, xs) => iter(xs, Cons(x, acc))
      }
    iter(this.reverse, ys)
  }

  /**
   * Parcurge Lista de la cap la coadă și apelează
   * funcția dată pentru fiecare element.
   */
  def foreach[U](f: T => U): Unit = {
    @tailrec
    def iter(xx: List[T]): Unit =
      xx match {
        case Nil => ()
        case Cons(x, xs) => {
          f(x)
          iter(xs)
        }
      }
    iter(this)
  }

}

case class Cons[+T](head: T, tail: List[T]) extends List[T]
case object Nil extends List[Nothing]

object List {
  /** Constructor pentru List */
  def apply[T](elems: T*): List[T] =
    elems.reverse.foldLeft(empty[T]) {
      case (Nil, head) => Cons(head, Nil)
      case (tail, head) => Cons(head, tail)
    }

  /** Returnează o Listă goală */
  def empty[T]: List[T] = Nil
}
